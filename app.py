from flask import Flask, request, Response
from botbuilder.schema import Activity
from botbuilder.core import BotFrameworkAdapter,BotFrameworkAdapterSettings

import asyncio

from echobot import EchoBot

app = Flask(__name__)
loop = asyncio.get_event_loop()

botadaptersettings=BotFrameworkAdapterSettings("","")
botadapter = BotFrameworkAdapter(botadaptersettings)

ebot = EchoBot()

#POST is the message
@app.route("/api/messages",methods=["POST"])
def messages():
	#checking if HTTP file format is JSON or not
	if "application/json" in request.headers["context-type"]:
		#reading the JSON message
		jsonmessage = request.json
	else:
		#unsupported media type 415
		return Response(status=415)

	activity = Activity().deserialize(jsonmessage)

	async def turn_call(turn_context):
		await ebot.on_turn(turn_context)

	task = loop.create_task(botadapter.process_activity(activity,"",turn_call))
	loop.run_until_complete(task)
	

if __name__ == '__main__':
	app.run('localhost',3978)